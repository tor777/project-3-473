package edu.luc.project3.proxy;

import java.lang.reflect.Proxy;

public class VehicleProxy {
		 
		  public static void main(String[] args) {
		    Vehicle realVehicle = new FerrariFF();
		    Vehicle proxy = (Vehicle) Proxy.newProxyInstance(realVehicle.getClass()
		        .getClassLoader(), realVehicle.getClass().getInterfaces(),
		        new VehicleInvocationHandler(realVehicle));
		    proxy.getPrice();
		  }
		}
