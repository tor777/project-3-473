package edu.luc.project3.proxy;

public interface Vehicle {

	public void getPrice();
}
