package edu.luc.project3.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


public class VehicleInvocationHandler implements InvocationHandler {
	//  private Object realVehicle;

	public VehicleInvocationHandler(Object realVehicle) {
	    this.realVehicle = realVehicle;
	  }
	 
	  public Object invoke(Object proxy, Method m, Object[] args) {
	    Object result = null;
	    try {
	      result = m.invoke(realVehicle, args);
	    } catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    return result;
	  }
	 
	  private Object realVehicle = null;
	}
	
	

